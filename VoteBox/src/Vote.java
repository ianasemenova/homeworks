
public class Vote implements Comparable<String>{
	
	private String name;
	
	public Vote(String aName) {
		name = aName;
	}
	
	@Override
	public int compareTo(String strToCompare) {
		return name.compareTo(strToCompare);
	}
	
	@Override
	public int hashCode() {
		final int price = 31;
		int result = 1;
		result = price * result + name.hashCode();
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Vote other = (Vote) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "Vote [name="+ name + "]";
	}
	
	public String getName() {
		return name;
	}
	public void setName(String newName) {
		name = newName;
	}

}
