public class CharSequence {
	
	public void printSequence(int m, int n){
		while(m<=n) {
			System.out.print(m+" ");
			m++;
		}
	}

	public static void main(String[] args) {
		
		CharSequence pokagy = new CharSequence();
		pokagy.printSequence(3, 10);
	}

}
