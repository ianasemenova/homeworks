public class Hrenj {
	
	public void printSequence(int n){
		int pred=0;
		int next=1;
		int count=0;
		
		while(count++<n){
			System.out.print(next+" ");
			
			next+=pred; 
			pred=next-pred;
		}
	}

	public static void main(String[] args) {
		Hrenj sequence = new Hrenj();
		sequence.printSequence(5);
		

	}

}
