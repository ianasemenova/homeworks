
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


public class Sort {
 
	public static void main(String[] args) {
		List<Cars> carsList = new ArrayList<Cars>();
		Cars obj1 = new Cars();
		obj1.setColor("zelenyj");
		obj1.setBrand("zaz");
		obj1.setWheelbaseLength(new Integer(316));
		carsList.add(obj1);
		
		Cars obj2=new Cars();
		obj2.setColor("temnoZelenyj");
		obj2.setBrand("niva");
		obj2.setWheelbaseLength(new Integer(55));
		carsList.add(obj2);
		
		Cars obj3 = new Cars();
		obj3.setColor("svetloZelenyj");
		obj3.setBrand("volga");
		obj3.setWheelbaseLength(new Integer(38));
		carsList.add(obj3);
		
		Collections.sort(carsList, new Comparator<Cars>() {
	        @Override
	        public int compare(Cars a1, Cars a2) {
	            return a2.wheelbaseLength.compareTo(a1.wheelbaseLength);
	        }
		});
		
		/*for (Cars car:carsList) {
			System.out.println(car.wheelbaseLength);
		}*/
		
		Collections.sort(carsList, new Comparator<Cars>() {
	        @Override
	        public int compare(Cars a1, Cars a2) {
	            return a1.color.compareTo(a2.color);
	        }
		});
		
		/*for (Cars car:carsList) {
			System.out.println(car.color);
		}*/
		Collections.sort(carsList, new Comparator<Cars>() {
	        @Override
	        public int compare(Cars a1, Cars a2) {
	            return a1.brand.compareTo(a2.brand);
	        }
		});
		
		for (Cars car:carsList) {
			System.out.println(car.brand + ' '+ car.color + ' ' + car.wheelbaseLength);
		}
		
		
	    
		

	}
public static class Cars/* implements Comparable<Cars>*/{
	protected String color;
	protected String brand;
	protected Integer wheelbaseLength ;
	
   public void setColor(String tsvet){
	   color=tsvet;
   }
   public void setBrand(String marka){
	   brand=marka;
   }
   public void setWheelbaseLength(Integer dlinnaBasy){
	   wheelbaseLength=dlinnaBasy;
   }
   public String getColor(){
	   return color;
   }
   public String getBrand(){
	   return brand;
   }
   public Integer wheelbaseLength(){
	   return wheelbaseLength;
   }
	/*@Override
	public int compareTo(Cars anotherCar) {
		return wheelbaseLength.compareTo(anotherCar.wheelbaseLength);
	}*/
}
}
